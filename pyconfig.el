;;; Packages
;;;; C
(use-package conda
  
  :defer t
  :config
  ;; (when (equal system-type 'darwin)
  ;;   (custom-set-variables
  ;;    '(conda-anaconda-home "/opt/miniconda3"))
  ;;   (setq conda-env-home-directory "/opt/miniconda3"
  ;;         conda-env-subdirectory "envs"))
  (when (equal system-name "dliden-mbp.local")
    (custom-set-variables
     '(conda-anaconda-home "/opt/homebrew/Caskroom/miniforge/base"))
    (setq conda-env-home-directory "/opt/homebrew/Caskroom/miniforge/base"
          conda-env-subdirectory "envs"))
  ;; if you want interactive shell support, include:
  (conda-env-initialize-interactive-shells)
  ;; if you want eshell support, include:
  (conda-env-initialize-eshell)
  ;; if you want auto-activation (see below for details), include:
  ;;(conda-env-autoactivate-mode nil)
  )

(use-package counsel-pydoc
  :disabled t
  )
;;;; E
;;;; F
(use-package flycheck
  
  :defer t)
;;;; J
(use-package jupyter
 ;; :straight nil
  :config
  (defun my/jupyter-refresh-kernelspace ()
    "Refresh Jupyter Kernelspecs"
    (interactive)
    ;; https://github.com/emacs-jupyter/jupyter/pull/423
    ;; the setenv should not be needed once that is merged
    (setenv "PYDEVD_DISABLE_FILE_VALIDATION" "1")
    (jupyter-available-kernelspecs t))
      )

;;;; L
;;;; P
;; Config builtin python
;; (use-package python
;;   :config
;;   (setq python-indent-guess-indent-offset-verbose nil)
;;   (cond
;;    ((executable-find "jupyter")
;;     (progn
;;       (setq python-shell-interpreter "jupyter"
;;             python-shell-interpreter-args "console --simple-prompt"
;;             python-shell-prompt-detect-failure-warning nil)
;;       (add-to-list 'python-shell-completion-native-disabled-interpreters "jupyter")))
;;    ((executable-find "python3")
;;     (setq python-shell-interpreter "python3"))
;;    (t
;;     (setq python-shell-interpreter "python"))))

(use-package
 python
 :config
 (setq
  python-indent-guess-indent-offset-verbose nil
  python-shell-interpreter-args "")
 (cond
  ((executable-find "python3")
   (setq python-shell-interpreter "python3"))
  (t
   (setq python-shell-interpreter "python")))
 (setq python-check-command "ruff check")
 (with-eval-after-load 'eglot
   (add-to-list
    'eglot-server-programs
    `(python-mode .
      ,(eglot-alternatives
        '(("ruff" "server") ("basedpyright-langserver" "--stdio")))))
   (add-to-list
    'eglot-server-programs
    `(python-ts-mode .
                     ,(eglot-alternatives
        '(("ruff" "server") ("basedpyright-langserver" "--stdio")))))
   (add-hook 'after-save-hook 'eglot-format)))

(use-package python-black
  :demand t
  :after python
  :hook (python-mode . python-black-on-save-mode-enable-dwim))

(use-package pyvenv
  :defer t
  :config
  (add-hook 'pyvenv-post-activate-hooks (lambda ()
                                          (pyvenv-restart-python)))
  :hook (python-mode . pyvenv-mode))
        

;; To start, largely taking this from doom emacs config

;; Defer garbage collection further back in the startup process
(setq gc-cons-threshold most-positive-fixnum)

;; prevent UI elements from loading
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;; According to doom emacs, resizing emacs frame based on resizing fonts can be expensive.
(setq frame-inhibit-implied-resize t)

;; Ignore X resources; its settings would be redundant with the other settings
;; in this file and can conflict with later config (particularly where the
;; cursor color is concerned).
(advice-add #'x-apply-session-resources :override #'ignore)

;; Prevent unwanted runtime builds; packages are compiled ahead-of-time when
;; they are installed and site files are compiled when gccemacs is installed.
;; (setq comp-deferred-compilation nil)

;; decoration
(when (>= emacs-major-version 29)
  (add-to-list 'default-frame-alist '(undecorated-round . t)))

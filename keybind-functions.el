;;; Buffers
;; Kill Other Buffers
;; all elisp lists are ordered pairs; cdr kills the second of the ordered pair.
;; in this case, the second element is all other buffers.
(defun kill-other-buffers() 
  (interactive)                                                                   
    (mapc 'kill-buffer (cdr (buffer-list (current-buffer)))))

;;; Dasboard
;; initiates dashboard and goes to dashboard
(defun go-home ()
  (interactive)
  (dashboard-insert-startupify-lists)
  (switch-to-buffer "*dashboard*")) 


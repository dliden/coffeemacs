;; -*- outshine-startup-folded-p: t; -*-

;;; Helper Functions to load before package
(defun load-gptel-prompts (filename)
 "Load GPTel prompts from org file at FILENAME.
Each level 1 heading becomes a prompt name, with its content as the directive."
 (interactive "fPrompts file: ")
 (let ((prompts (with-current-buffer
                    (find-file-noselect filename)
                  (let ((prompt-list nil))
                    (org-map-entries
                     (lambda()
                       (let ((name (org-get-heading t t t t))
                             (content (substring-no-properties (org-get-entry))))
                         (push (cons (intern name) content) prompt-list)))
                     "LEVEL=1")
                    prompt-list))))
   (setopt gptel-directives prompts)))


;;; Main use-package call
(use-package gptel
  :init
  ;; directives
  (load-gptel-prompts "~/org/gptel-prompts.org")
 :config
 (setq
  gptel-model 'gemini-2.0-flash
  gptel-backend
 (gptel-make-gemini
  "Gemini"
  :stream t
  :key
  #'(lambda ()
      (auth-source-pick-first-password
       :host "generativelanguage.googleapis.com"
       :source "~/.authinfo")))
  gptel-prompt-prefix-alist
  '((markdown-mode . "### ")
    (org-mode . "@user\n")
    (text-mode . "User -> "))
  gptel-response-prefix-alist
  '((markdown-mode . "\n**Response:**\n")
    (org-mode . "@assistant\n")
    (text-mode . "\nResponse:\n"))
  gptel-default-mode 'org-mode)

 ;; Additional backend options
 (gptel-make-ollama
  "Ollama"
  :host "localhost:11434"
  :stream t
  :models '(llama3.2))

 (gptel-make-anthropic
     "Anthropic"
   :stream t
   :key
   #'(lambda ()
       (auth-source-pick-first-password
        :host "api.anthropic.com"
        :source "~/.authinfo")))

 (gptel-make-openai
  "OpenAI"
  :stream t
  :key
  #'(lambda ()
      (auth-source-pick-first-password
       :host "api.openai.com"
       :source "~/.authinfo")))

 (gptel-make-gemini
  "Gemini"
  :stream t
  :key
  #'(lambda ()
      (auth-source-pick-first-password
       :host "generativelanguage.googleapis.com"
       :source "~/.authinfo")))
 
 (add-hook 'gptel-post-response-functions 'gptel-end-of-response)
 (add-hook 'gptel-post-stream-hook 'gptel-auto-scroll)
 )


;;; Tools

;; Heavily inspired by https://github.com/positron-solutions/youtube/blob/master/lunch/09-retreival-augmented-macros/demo-gptel-config.el

;;;; Elisp Evaluation

(defun dl--gptel-elisp-eval (expr)
  (format "%S" (eval (read expr))))

(gptel-make-tool
 :function #'dl--gptel-elisp-eval
 :name "elisp_eval"
 :confirm t
 :include t
 :category "code execution"
 :args '(( :name "expr"
           :type string
           :description "A single elisp sexp to evaluate, not enclosed in additional quotes."))
 :description "Evaluate a single, standalone Elisp expression and return the result as a string.

Usage Notes:

- One Expression: Only the first expression is evaluated. Do /not/ combine expressions with progn; use separate calls.
- Standalone: Expression should be self-contained.
- Return Value: Result is a string (Elisp's %S format): strings are escaped, symbols/numbers are readable, complex objects may be #<hash-notation>

Use Cases: Calculations ((+ 2 2)), variable lookup (major-mode), setting changes ((setq auto-save-interval 10)), etc..

Limitations: Avoid complex programs. If the task is too complex, break it down into simpler Elisp expressions and call this tool multiple times")

;;;; Introspection

(gptel-make-tool
 :function (lambda (function)
             (documentation (intern function)))
 :name "elisp_documentation"
 :confirm t
 :include t
 :category "introspection"
 :args '(( :name "function"
           :type string
           :description "The function or symbol to get documentation for."))
 :description "Return the documentation string of FUNCTION."
 )

(defun dl--gptel-inspect-variable (symbol_name)
  "Return a string containing custom variable documentation (if any)
and the value of a symbol."
  (let* ((symbol (intern symbol_name))
         (custom-doc (condition-case nil
                         (custom-variable-documentation symbol)
                       (error nil)))  ; Handle case where it's not a custom var
         (value (symbol-value symbol))
         (result (if custom-doc
                     (concat "Description:\n"
                             custom-doc
                             "\n\nValue:\n"
                             (format "%S" value))
                   (concat "Value: " (format "%S" value)))))
    result))

(gptel-make-tool
 :function #'dl--gptel-inspect-variable
 :name "elisp_inspect_symbol"
 :confirm t
 :include t
 :category "introspection"
 :args '(( :name "symbol_name"
           :type string
           :description "The name of the symbol to inspect."))
 :description "Return custom variable documentation (if available) and the value of a symbol."
 )

(defun dl--gptel-command-completions (search_string)
  (require 'orderless)
  (string-join (orderless-filter search_string obarray #'commandp) "\n"))


(gptel-make-tool
 :function #'dl--gptel-command-completions
 :name "elisp_command_completions"
 :confirm t
 :include t
 :category "introspection"
 :args '(( :name "search_string"
           :type string
           :description "String to search for in command names.  The search is flexible and can match parts of words in any order. For example, you could search 'fun apropos' to look for apropos commands related to functions."))
 :description "Return a newline-separated list of Emacs commands that match the search string.  This is useful for discovering commands related to a specific topic. After using this tool, if relevant, you can use `elisp_documentation' to learn more about a relevant command."
 )

;;;;; Info Manual Tools
(defun pmx--gptel-manual-names ()
  (json-serialize (vconcat (info--filter-manual-names
                            (info--manual-names nil)))))

(defun pmx--gptel-manual-list-nodes (name)
  (json-serialize
   (vconcat
    (mapcar #'car (Info-build-node-completions name)))))

(defun pmx--gptel-manual-node-contents (manual node)
  (condition-case err
      (progn
        (save-window-excursion
          (Info-goto-node (format "(%s)%s" manual node))
          (buffer-substring-no-properties (point-min) (point-max))))
    (user-error
     (error (error-message-string err)))))

(defun pmx--gptel-symbol-in-manual (symbol)
  (require 'helpful)
  (when-let* ((symbol (intern-soft symbol))
              (_completion (helpful--in-manual-p symbol)))
    (save-window-excursion
      (info-lookup 'symbol symbol #'emacs-lisp-mode)
      (buffer-substring-no-properties (point-min) (point-max)))))

(gptel-make-tool
 :function #'pmx--gptel-manual-names
 :name "manual_names"
 :include t
 :category "introspection"
 :args nil
 :description "Return a list of available manual names.
Call this tool in order to determine if a particular manual is
available.  This can also help determine which packages are available on
the user's Emacs.  This tool is a good starting point for general
questions about Emacs, Elisp, and common built-in packages.

Manuals are usually named the same as the symbol of the package prefix
that they document.  The Common Lisp manual is called \"cl\".  The Emacs
Lisp manual is called \"elisp\".

You will usually follow this call with a subsequent call to
`manual_nodes' in order to see the sections in the manual, which are
somewhat like a summary.  This call is extremely cheap and should be
used liberally.")

(gptel-make-tool
 :function #'pmx--gptel-manual-list-nodes
 :name "manual_nodes"
 :include t
 :category "introspection"
 :args '(( :name "manual"
           :type string
           :description "The name of the manual.
Examples include \"cl\", \"elisp\", or \"transient\"."))
 :description "Retrieve a listing of topic nodes within MANUAL.
Return value is a list of all nodes in MANUAL.  The list of topic nodes
provides a good summary of MANUAL.

MANUAL is one of the results returned from `manual_names'.  If you are
sure a manual exists, you may skip first calling `manual_names'.  When
you believe MANUAL exists, this tool is very useful to find places to
broaden your search.

You will usually follow this call with a subsequent call to
`manual_node_contents' to view the actual full contents of a node in the
manual.  This call is extremely cheap and should be used liberally.

In the Elisp manual, you can find more answers about code and
implementations that a programmer can used to deeply customize.  The
Emacs manual contains descriptions about built-in features and behavior
that can be used to understand the context for what is being programmed
in the Elisp manual.")

(gptel-make-tool
 :function #'pmx--gptel-manual-node-contents
 :name "manual_node_contents"
 :include t
 :category "introspection"
 :args '(( :name "manual_name"
           :type string
           :description "The name of MANUAL.
Examples manuals include \"cl\", \"elisp\", or \"transient\".")
         ( :name "node"
           :type string
           :description "The name of the NODE in a MANUAL.
Example nodes from the elisp manual include \"Records\" or \"Sequences
Arrays \ Vectors\"."))
 :description "Retrieve the contents of NODE in MANUAL.
The return value is the full contents of NODE in MANUAL.  Contents
include high-level grouping of related functions and variables.  Hidden
behavior is described.  This tool is awesome!  You should try to call it
all the time.

Pay attention to the entries in the Menu.  You can do recursive look-ups
of more specific manual sections.  Example menu:

* Menu:

* Good Node::
* A Node::

You can recursively look up \"Good Node\" and other relevant menu nodes
in this same MANUAL.  Sometimes there are links, such as, \"*Note
Narrowing::.\".  \"Narrowing\" is a node in this example.  Use this tool
recursively.

If both Elisp and Emacs manuals are available, open both but prefer Elisp manual
style language and content.")

;;;;; File System Introspection


(defun dl--gptel-list-directory (dirname &optional verbose)
  (let* ((command (if verbose
                      (concat "ls -l " (shell-quote-argument (expand-file-name dirname)))
                    (concat "ls " (shell-quote-argument (expand-file-name dirname))))))
    (shell-command-to-string command)))

(gptel-make-tool
 :function #'dl--gptel-list-directory
 :name "list_directory"
 :confirm t
 :include t
 :category "file system"
 :args '(( :name "dirname"
           :type string
           :description "The directory to list.")
         ( :name "verbose"
           :type boolean
           :description "Whether to use verbose listing (like =ls -l=)."
           :optional t
           :default nil))
 :description "List the files in a directory.  Equivalent to the =ls= command, capturing output as a string."
 )

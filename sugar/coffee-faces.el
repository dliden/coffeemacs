;;; coffee-faces --- face settings for coffeemacs
;;; Significantly inspired by
;;;    https://github.com/rougier/nano-emacs/blob/master/nano-faces.el
;;; License:
;; -----------------------------------------------------------------------------
;; TBC
;; -----------------------------------------------------------------------------
;;; Commentary

;;
;; This file defines the basic faces used in coffeemacs

;; Function for overwriting a face with one of the faces defined here
(defun set-face (face style)
  "Reset FACE and make it inherit STYLE."
  (set-face-attribute face nil
                      :foreground 'unspecified :background 'unspecified
                      :family     'unspecified :slant      'unspecified
                      :weight     'unspecified :height     'unspecified
                      :underline  'unspecified :overline   'unspecified
                      :box        'unspecified :inherit    style))

(defgroup coffee '()
  "faces and colors for coffeemacs"
  :group 'emacs)

(defcustom coffee-font-family-mono "Iosevka Term"
  "Name of the monospace font family to use.
   Defaults to Iosevka Fixed."
  :group 'coffee
  :type 'string)


(defcustom coffee-font-size 14
  "Default coffeemacs font size value in pt units"
  :group 'coffee
  :type 'integer)

(defface coffee-face-default nil
  "Default coffeemacs face for general use"
  :group 'coffee)

(defface coffee-face-variable-pitch nil
  "Name of proportional font family to use
Defaults to Iosevka Aile (previously ETBembo)."
  :group 'coffee)

(defface coffee-face-strong nil
  "Face used for e.g. org headlines and other structural components"
  :group 'coffee)

(defface coffee-face-faded nil
  "Faded face is for information that are less important.
It is made by using the same hue as the default but with a lesser
intensity than the default. It can be used for comments,
secondary information and also replace italic (which is generally
abused anyway).

  Need to set custom colors before doing anything with this"
  :group 'coffee)

(defun coffee-faces ()
  "Derive face attributes from coffee group values"
  (set-face-attribute 'coffee-face-default nil
                      :family coffee-font-family-mono
                      :height (* coffee-font-size 10))
  (set-face-attribute 'coffee-face-variable-pitch nil
                      :family "Iosevka Aile"
                      :height (* coffee-font-size 10))
  (set-face-attribute 'coffee-face-strong nil
                      :family "Iosevka Etoile"
                      :height (* coffee-font-size 10)
                      :weight 'bold))

(defun coffee-theme--basics()
  "Apply default theme options"
  (setq default-frame-alist
        (append (list
                 '(font . "Iosevka Term:style=Light:size=15")
                 '(height . 50)
                 '(width . 81)
                 '(vertical-scroll-bars . nil)
                 '(left-fringe . 1)
                 '(right-fringe . 1)
                 '(internal-border-width . 12)
                 '(tool-bar-lines . 0)
                 '(menu-bar-lines . 0))))
  ;; Vertical window divider
  (setq window-divider-default-right-width 0)
  (setq window-divider-default-places 'right-only)
  (window-divider-mode 1))

(defun coffee-theme--org()
  "Apply org-mode themes"
  (set-face 'org-date 'coffee-face-default)
  (set-face 'org-default 'coffee-face-variable-pitch)
  (set-face 'org-drawer 'coffee-face-default)
  (set-face 'org-level-1 'coffee-face-strong)
  (set-face 'org-level-2 'coffee-face-strong)
  (set-face 'org-level-3 'coffee-face-strong)
  (set-face 'org-level-4 'coffee-face-strong)
  (set-face 'org-level-5 'coffee-face-strong)
  (set-face 'org-level-6 'coffee-face-strong)
  (set-face 'org-level-7 'coffee-face-strong)
  (set-face 'org-level-8 'coffee-face-strong)
  (set-face 'org-property-value 'coffee-face-default)
  (set-face 'org-special-keyword 'coffee-face-default)
  )
(provide 'coffee-faces)

;;; Miscellaneous
;    (speedbar-add-supported-extension ".R")
;;; Packages
;;;; E
(use-package ess
  :ensure t
  :init (require 'ess-site)
  :defer t
;  :disabled t
  :config
  (setq 
        ess-r-backend 'lsp
        ess-style 'RStudio
        ess-use-flymake nil)
  )
(add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-mode))

;;; General
;; suppress warning about cl deprecation
(setq byte-compile-warnings '(cl-functions))
;;;; load config files
;; Custom Functions
(setq keybind-functions-file (expand-file-name "keybind-functions.el" user-emacs-directory))
(setq python-config-file (expand-file-name "pyconfig.el" user-emacs-directory))
(setq ess-config-file (expand-file-name "essconfig.el" user-emacs-directory))
(setq org-config-file (expand-file-name "orgconfig.el" user-emacs-directory)) 
(setq mac-config-file (expand-file-name "macconfig.el" user-emacs-directory))
(setq gptel-config-file (expand-file-name "gptel-config.el" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "sugar/" user-emacs-directory))



;;;; user details
(setq-default user-full-name "Daniel James Liden"
              user-mail-address "dliden@pm.me")

;;;; Sane Defaults
;; Garbage Collection
(setq gc-cons-threshold 100000000)

;; read process output increase
(setq read-process-output-max (* 1024 1024))

;; Visual Line Mode
(global-visual-line-mode nil)

;; delete excess backup versions
(setq delete-old-versions -1 )

;; use version control in backups: number the backup files
(setq version-control t )

;; make backups even in version cotrolled dir
(setq vc-make-backup-files t)

;; set directory for backup files
(setq backup-directory-alist
      `((".*" . ,(expand-file-name "backups/" user-emacs-directory))))

;; follow symlinks without asking
(setq vs-follow-symlinks t )

;; silence bell
(setq ring-bell-function 'ignore )

;; Use utf-8
(setq coding-system-for-read 'utf-8 )
(setq coding-system-for-write 'utf-8 )

;; More recentf targets
(recentf-mode 1)
(setq recentf-max-saved-items 30
      recentf-max-menu-items 30)

;; Don't warn about large files
(setq large-file-warning-threshold nil)

;; WindMove (shift+arrow to change windows)
;;(windmove-default-keybindings)

;; y/n for  answering yes/no questions
(fset 'yes-or-no-p 'y-or-n-p)

;;;; Visual setup
;; Disable some Defaults
;(menu-bar-mode -1)
;;;; Decoration

(toggle-scroll-bar -1)
(tool-bar-mode -1)
(blink-cursor-mode -1)
(setq inhibit-startup-screen t)

;; Enable Useful Visual Indicators
(global-hl-line-mode -1)
(setq display-line-numbers-width 5)
;; tab spacing
(setq-default tab-width 4
              indent-tabs-mode nil
              column-number-mode t
              )

;; larger fringe
(set-fringe-mode 10)

;; line numbers (Relative)
(setq display-line-numbers-type 'visual )
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; pixel-wise frame resize
(setq frame-resize-pixelwise t)

;; fill-column
(setq auto-fill-mode nil)
(setq fill-column 80)

;; misc
(setq cursor-in-non-selected-windows nil)

;;;; Scrolling Behavior
;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
;(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq scroll-conservatively 10000)
(setq auto-window-vscroll nil)
(setq use-dialog-box nil) ;; Disable dialog boxes since they weren't working in Mac OSX
(setq vertical-scroll-bar nil)


;;;; Encryption options
(setf epa-pinentry-mode 'loopback)
; https://emacs.stackexchange.com/questions/27841/unable-to-decrypt-gpg-file-using-emacs-but-command-line-gpg-works
;;;; Modeline and Header Line
(setq-default mode-line-format
   '((:propertize
      (mode-line-modified mode-line-remote)
      display
      (min-width
       (5.0)))
     "   "
     mode-line-position evil-mode-line-tag
     (vc-mode vc-mode)
     "  " mode-name mode-line-misc-info mode-line-end-spaces)
   flymake-mode-line-counters) 

(setq-default header-line-format '(mode-line-frame-identification
                                   mode-line-buffer-identification))

;;;; Dired

(add-hook 'dired-mode-hook #'dired-hide-details-mode)

;;;; Registers

(setopt register-use-preview 't)

;;; Package management
;; set up package archives and use-package package
;;(require 'package)

;; Do not load packages at startup
;;(setq package-enable-at-startup nil)

;; Set Package Archives
;;(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
;;                         ("gnu"       . "http://elpa.gnu.org/packages/")
;;                         ("melpa"     . "https://melpa.org/packages/")))

;;(when (equal system-type 'darwin)
;;(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
;;                         ("gnu"       . "http://elpa.gnu.org/packages/")
;;                         ("melpa"     . "http://melpa.org/packages/"))))

;; Load Emacs Lisp packages, and activate them.
;;(if(<= (string-to-number emacs-version) 29)
;;    (package-initialize)) ;;supposedly unnecessary in emacs 27 but cant find use-package without it for some reason.

;; Bootstrap 'use-package'
;;(unless (package-installed-p 'use-package) ; unless it is already installed
;;  (package-refresh-contents) ; updage packages archive
;;  (package-install 'use-package)) ; and install the most recent version of use-package

;; Native compile if applicable
(if(>= (string-to-number emacs-version) 28.0)
      (setq package-native-compile t)) 

;; Bootstrap straight
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; load use-package
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
;;(require 'use-package)

;; Mac-Specific Config
(when (equal system-type 'darwin)
  (load mac-config-file))

;;; Packages to Include
;;;; Early Load
;; This section is for packages that must be loaded before
;; other packages, and thus are removed from the alphabetical
;; order maintained below.

(use-package
 exec-path-from-shell
 :config
 ;;;; Trying to resolve macos sequoia issue
 (when (memq window-system '(mac ns x))
   (exec-path-from-shell-initialize)))

; required by any use-package with :general keyword
(use-package general
  :straight t
  )
;;;; A

(use-package ace-window
  :config
  (setq aw-dispatch-always t)
  :general
  ("M-o" #'ace-window))

(use-package aidermacs
  :straight (:host github :repo "MatthewZMD/aidermacs" :files ("*.el"))
  :config
  (setq aidermacs-default-model "sonnet")
  (global-set-key (kbd "C-c A") 'aidermacs-transient-menu)
  ; Enable minor mode for Aider files
  (aidermacs-setup-minor-mode)
  ; See the Configuration section below
  (setq aidermacs-auto-commits t
        aidermacs-use-architect-mode t
        aidermacs-backend 'vterm)
  ; Ensure emacs can access *_API_KEY through .bashrc or setenv
  (setenv "ANTHROPIC_API_KEY"
       (auth-source-pick-first-password
        :host "api.anthropic.com"
        :source "~/.authinfo")))

(use-package avy 
  :general
  ("M-j" #'avy-goto-char-timer))
(use-package all-the-icons )
(use-package all-the-icons-completion
  :straight t
  :hook (marginalia-mode . all-the-icons-completion-marginalia-setup)
  :init
  (all-the-icons-completion-mode))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

;;;; C
(use-package citar
  :after (marginalia all-the-icons)
  
  :straight t
  :general
  ("C-c b" #'citar-insert-citation)
  (:keymaps 'minibuffer-local-map
            "M-b" #'citar-insert-preset)
  :config
  ;; https://github.com/minad/consult/issues/567
  ;;(advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)
  :custom
  (citar-bibliography '("~/org/slip-box/bibliography/references.bib")))

;; (use-package shell-maker
;;   :straight (:host github :repo "xenodium/chatgpt-shell" :files ("shell-maker.el")
;;                    :branch "main"))

;; (use-package chatgpt-shell
;;   :after (shell-maker)
;;   :straight (:host github :repo "xenodium/chatgpt-shell" :files ("chatgpt-shell.el")
;;                    :branch "main")
;;   :config
;;   (setq chatgpt-shell-openai-key
;;       (lambda ()
;;         (auth-source-pick-first-password :host "api.openai.com"))))

;; (use-package ob-chatgpt-shell
;;   :straight (:host github :repo "xenodium/chatgpt-shell" :files ("ob-chatgpt-shell.el") :branch "main")
;;   :ensure t)

(use-package corfu
  :straight (:files (:defaults "extensions/*.el"))
  :init
  (global-corfu-mode)
  :general
  (:keymaps 'corfu-map
            :states 'insert
            "C-n" #'corfu-next
            "C-p" #'corfu-previous
            "<escape>" #'corfu-quit
            "<return>" #'corfu-quit
            "TAB" #'corfu-insert
            "M-d" #'corfu-show-documentation
            "M-l" #'corfu-show-location)
  :config
  (setq corfu-auto t
        corfu-quit-no-match 'separator
        tab-always-indent 'complete
        corfu-auto-prefix 2
        corfu-min-width 50
        corfu-max-width corfu-min-width
        corfu-echo-documentation t))

;; Add extensions
(use-package cape
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("M-p p" . completion-at-point) ;; capf
         ("M-p t" . complete-tag)        ;; etags
         ("M-p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("M-p h" . cape-history)
         ("M-p f" . cape-file)
         ("M-p k" . cape-keyword)
         ("M-p s" . cape-symbol)
         ("M-p a" . cape-abbrev)
         ("M-p i" . cape-ispell)
         ("M-p l" . cape-line)
         ("M-p w" . cape-dict)
         ("M-p \\" . cape-tex)
         ("M-p _" . cape-tex)
         ("M-p ^" . cape-tex)
         ("M-p &" . cape-sgml)
         ("M-p r" . cape-rfc1345))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  ;; these are those that will use auto-complete/tab completion.
  ;; ((add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  ;;(add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  ;; Silence the pcomplete capf, no errors or messages!
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

  ;; Ensure that pcomplete does not write to the buffer
  ;; and behaves as a pure `completion-at-point-function'.
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)
)

(use-package consult
  :straight t
  :general
  (general-define-key
   :states '(normal visual insert emacs)
   "C-s" '(consult-line :which-key "consult line"))
  (general-define-key
   :states '(normal visual insert emacs)
   :prefix "SPC"
   :non-normal-prefix "M-SPC"
   ;; Buffers
   "bs" '(consult-buffer :which-key "switch buffer")
   "bl" '(consult-bookmark :which-key "list bookmarks")
  ))

(use-package consult-notes
  :straight (:type git :host github :repo "mclear-tools/consult-notes")
  :commands (consult-notes
             consult-notes-search-in-all-notes
             consult-notes-org-roam-find-node
             consult-notes-org-roam-find-node-relation)
  :config
;; to narrow, type the key followed by space.
  (setq consult-notes-sources '(("Denote"  ?d  "~/org/denotes")
                                ("Fleeting"  ?f  "~/org/denotes/fleeting")
                                ("Literature"  ?l  "~/org/denotes/literature")
                                ("Permanent"  ?p  "~/org/denotes/permanent")
                                ("Personal"  ?e  "~/org/denotes/personal")
                                ("Journal"  ?j  "~/org/denotes/journal")
                                ("Databricks"  ?D "~/org/denotes/databricks/")
                                ("AI Chats"  ?a "~/org/denotes/ai-chats/")
                                ("Hub"  ?h  "~/org/denotes/hubs"))))

(use-package consult-projectile
    :straight (consult-projectile :type git :host gitlab :repo "OlMon/consult-projectile" :branch "master"))

;;;; D
(use-package denote
  :config
  (setq denote-directory "~/org/denotes"
        denote-known-keywords '("personal" "history" "study" "work")
        denote-backlinks-show-context t
        ;; eventually separate this into custom functions for
        ;; each type of note. Also--quarto?? ipynb?? Pluto??
        ;; is it possible to make this more type-agnostic?
  denote-prompts '(subdirectory file-type title keywords signature)))

(use-package diminish)

(use-package doom-themes
  :after (ef-themes)
  :config
  (defun my/apply-theme (appearance)
    "Load theme, taking current system APPEARANCE into consideration."
    (mapc #'disable-theme custom-enabled-themes)
    (pcase appearance
      ('light (ef-themes-load-random 'light))
      ('dark (ef-themes-load-random 'dark))))
  ;; options for modus themes here
  (setq modus-themes-mode-line '(accented borderless (padding
                                                      . 4) (height . 0.9))
        modus-themes-hl-line 'accented
        modus-themes-subtle-line-numbers t
        modus-themes-org-blocks 'gray-background
        modus-themes-italic-constructs t
        modus-themes-bold-constructs t
        modus-themes-syntax '(greeen-strings yellow-comments alt-syntax)
        modus-themes-headings '((1. (background overline variable-pitch rainbow)))
        modus-themes-completions nil
        modus-themes-hl-line nil
        modus-themes-fringes nil
        )

  (add-hook 'ns-system-appearance-change-functions #'my/apply-theme)

  )

;;;; E

(use-package eat)

(use-package ef-themes)

(use-package elisp-autofmt)

(use-package embark
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  (defun avy-action-embark (pt)
  (unwind-protect
      (save-excursion
        (goto-char pt)
        (embark-act))
    (select-window
     (cdr (ring-ref avy-ring 0))))
  t)
  (setf (alist-get ?. avy-dispatch-alist) 'avy-action-embark))

(use-package embark-consult
  :after (embark consult)
  :demand t ; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package evil
  
  :defer .1
  :init
  (setq evil-want-integration t)
  (setq evil-respect-visual-line-mode t)
  (setq evil-want-C-u-scroll nil)
  (setq evil-want-C-i-jump nil)
  (setq evil-undo-system 'undo-tree
        evil-ex-visual-char-range t)
  )
(use-package evil-terminal-cursor-changer
  
  :config
  (unless (display-graphic-p)
    (require 'evil-terminal-cursor-changer)
    (evil-terminal-cursor-changer-activate) ; or (etcc-on)
    )
  (setq evil-motion-state-cursor 'box)  ; █
  (setq evil-visual-state-cursor 'box)  ; █
  (setq evil-normal-state-cursor 'box)  ; █
  (setq evil-insert-state-cursor 'bar)  ; ⎸
  (setq evil-emacs-state-cursor  'hbar) ; _
  (evil-mode 1))

(load ess-config-file)
;;;;; Elfeed and Related
(use-package elfeed
  
  :config
  (global-set-key (kbd "C-x w") 'elfeed)
  (setq elfeed-curl-program-name "curl")
  )
(use-package elfeed-org
  
  :init
  (require 'elfeed-org)
  (elfeed-org)
  :config
  (setq rmh-elfeed-org-files (list "~/org/elfeed.org")))
  

;;;;; Esup startup profiling
(use-package esup
  
  :commands (esup))

;;;; G
(load keybind-functions-file)

;;;; H
(use-package helpful)
(use-package hydra
  :defer 2)
;;;; I
(use-package imenu-list
  
  :straight t
  )
;;;; J
(use-package julia-mode
  
  :defer t
  :disabled t)
(use-package julia-repl
  ;; https://github.com/tpapp/julia-repl
  
  :defer t
  :disabled t
  ;:hook
  ;(julia-mode . julia-repl-mode))
  )
(use-package julia-snail
  :straight t
  
  :defer t
  :after (vterm)
  ;; :hook
  ;; (julia-mode . julia-snail-mode)
  ;; invoke manually otherwise triggered in jupyter-julia

  )

(use-package julia-vterm
  
  :disabled t
  )

;;;; M
(use-package magit
  :straight t
  )

(use-package marginalia
  :straight t  
  :init
  (marginalia-mode)
  :general
  (:keymaps 'minibuffer-local-map
            "M-A" 'marginalia-cycle)
  :custom
  (marginalia-max-relative-age 0)
  (marginalia-align 'right))
(use-package markdown-mode
  
  :straight t
  :init (setq markdown-command "pandoc"))
;;;; O

(use-package olivetti)

(load org-config-file)
(use-package ob-http
  
  :defer t)
(use-package orderless
  :straight t
  
  :init
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))
(use-package outline
  :diminish (outline-minor-mode . "")
  :hook (prog-mode . outline-minor-mode)
  :config
  (defvar outline-minor-mode-prefix "\M-#"))

(use-package outshine
  
  :diminish;; (outline-minor-mode . "")
  :hook (outline-minor-mode . outshine-mode)
  :config
  )

;;;; P
(use-package page-break-lines
  )
(use-package pandoc-mode
  )
(use-package pdf-tools
  
  :config
  (pdf-tools-install)
  (pdf-loader-install)
 (setq-default pdf-view-display-size 'fit-page)
 (setq pdf-annot-activate-created-annotations t
       pdf-view-use-scaling t)
 :defer nil)

;; postgres
(use-package pg
  :straight (:host github :repo "emarsden/pg-el"))

(use-package pgmacs
  :straight (:host github :repo "emarsden/pgmacs"))

(use-package polymode
  
  :straight t
  )
(use-package poly-markdown
  
  :straight t)


(use-package projectile
  
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (setq projectile-completion-system 'ivy)
  :diminish)

;; pgp stuff
;;(require 'epa-file)
;;(custom-set-variables '(epg-gpg-program  "/opt/homebrew/Cellar/gnupg"))
;;(epa-file-enable)

;;;; Q
(use-package quarto-mode
  :straight t
  
  )
;;;; R
(use-package request
  
 )
;;;; S
(use-package savehist
  :config
  (setq savehist-file (locate-user-emacs-file "savehist")
        history-length 1000
        history-delete-duplicates t
        savehist-save-minibuffer-history t)
  :hook
  (after-init . #'savehist-mode))

(use-package sdcv  )

(use-package spacious-padding
  :config
 (setq spacious-padding-subtle-mode-line nil)
)

(use-package straight
             :custom (straight-use-package-by-default t))
;;;; T
;;;; U
(use-package undo-tree
  :diminish
  :init
  (global-undo-tree-mode 1)
  :custom
  (undo-tree-visualizer-diff t)
  (undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
  (undo-tree-visualizer-timestamps t)
  (undo-tree-show-minibuffer-help t)
  :config
  (define-key undo-tree-map (kbd "M-_") nil)  ; Unbind M-_
  :bind (:map undo-tree-map
         ("C-?" . undo-tree-redo)))

;;;; V
(use-package vertico
  
  :straight (vertico :files (:defaults "extensions/*")
                    :includes (vertico-directory))
  :general
  (:keymaps 'vertico-map
            "<tab>" #'vertico-insert  ; Insert selected candidate into text area
            "<escape>" #'minibuffer-keyboard-quit ; Close minibuffer
            ;; NOTE 2022-02-05: Cycle through candidate groups
            "C-M-n" #'vertico-next-group
            "C-M-p" #'vertico-previous-group
            "<backspace>" #'vertico-directory-delete-char
            "C-w" #'vertico-directory-delete-word
            "RET" #'vertico-directory-enter)
  :custom
  (vertico-count 13)
  (vertico-resize nil)
  :init
  (vertico-mode))

(use-package visual-fill-column)

(use-package vterm
  )
;;;; W
(use-package which-key
  
  :diminish which-key-mode
  :defer 2
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.3))

;;;; Y

(use-package yasnippet
  :diminish
  :config
  (setq yas-snippet-dirs '("~/coffeemacs/snippets/"))
  (yas-global-mode 1)
  )
;;; Load Other Files after Init
;;;;; Python
(load python-config-file)
(load gptel-config-file)


;;; Custom Functions
(defun insert-em-dash ()
  (interactive)
  (insert "—"))

(defun insert-en-dash ()
  (interactive)
  (insert "–"))

(global-set-key (kbd "M-_") 'insert-em-dash)
(global-set-key (kbd "C-M-_") 'insert-en-dash)



;;; Key Bindings
;; Overwrite some defaults
(general-define-key
 :states '(normal visual insert emacs)
 "M-y" 'consult-yank-pop
 "M-x" 'execute-extended-command ""
 )

(general-define-key
 :states '(normal visual insert emacs)
 :prefix "SPC"
 :non-normal-prefix "M-SPC"
 ;; Buffers
 "b" '(:ignore t :which-key "buffers")
 "bd" '(kill-this-buffer :which-key "delete buffer")
 "b C-d" '(kill-other-buffers :which-key "delete all other buffers")
 ;; Counsel
 "c" '(:ignore t :which-key "Consult")
 "cs" '(counsel-describe-symbol :which-key "Describe Symbol")
 "ce" '(counsel-find-file-extern :which-key "Open file externally")
 "cu" '(counsel-unicode-char :which-key "Unicode character")
 "cn" '(consult-notes :which-key "Consult Notes")
 "cp" '(consult-projectile :which-key "Consult Projectile")
 ;; Denote
 "d" '(:ignore t :which-key "Denote")
 "dn" '(denote-create-note :which-key "New Note")
 ;; Embark
 "e" '(:ignore t :which-key "embark")
 "ea" '(embark-act :which-key "Embark act")
 ;; Exiting
 "q" '(:ignore t :which-key "quit")
 "qq" '(save-buffers-kill-emacs :which-key "Save and Kill")
 "qz" '(suspend-emacs :which-key "suspend emacs")
 ;; Files
 "f" '(:ignore t :which-key "files")
 "fr" '(consult-recent-file :which-key "recent files")
 "fc" '(company-files :which-key "complete filename")
 ;; Python and Jupyter
 "j" '(:ignore t :which-key "python")
 "jk" '((lambda() (interactive) (org-babel-jupyter-aliases-from-kernelspecs)) :which-key "find jupyter kernels")
 "jv" '(pyvenv-workon :which-key "pick virtual environment")
 "jb" '(jupyter-org-insert-src-block :which-key "insert jupyter babel src block")
 "jn" '((lambda() (interactive) (shell-command "jupyter notebook --no-browser &")) :which-key "start jupyter notebook server")
;; Registers
 "r" '(:ignore t :which-key "registers")
 "rr" '(point-to-register :which-key "point to register")
 "rj" '(jump-to-register :which-key "jump to register")
;; Tabs
 "t" '(:ignore t :which-key "tabs")
 "tn" '(tab-bar-switch-to-next-tab :which-key "next tab")
 "tp" '(tab-bar-switch-to-prev-tab :which-key "previous tab")
 "tt" '(tab-bar-new-tab :which-key "new tab")
 "tx" '(tab-bar-close-tab :which-key "close tab")
 "tr" '(tab-bar-rename-tab :which-key "rename tab")
 "tf" '(find-file-other-tab :which-key "find file new tab")
 "tb" '(switch-to-buffer-other-tab :which-key "switch to buffer new tab")
 ;; Windows
 "w" '(:ignore t :which-key "windows")
 "wh" '(windmove-left :which-key "window left")
 "wl" '(windmove-right :which-key "window right")
 "TAB" '(other-window :which-key "other window")
 ;; Words
 "W" '(:ignore t :which-key "Dictionary & Spelling")
 "Ww" '(sdcv-search-pointer :which-key "Webster's 1913 current word")
 "Wc" '(ispell-word :which-key "spellcheck current word")
 "Wb" '(ispell-buffer :which-key "spellcheck current buffer")
 "Ws" '(sdcv-search-simple :which-key "lookup word")
 )

;; other keybindings
(bind-key "C-<wheel-up>" nil) ;; No text resize via mouse scroll
(bind-key "C-<wheel-down>" nil) ;; No text resize via mouse scroll

;;; setups after packages
;;;; modeline
(setq-default mode-line-format (cons '(:exec conda-env-current-name) mode-line-format))
(require 'coffee-faces)
(coffee-faces)
(coffee-theme--basics)
(coffee-theme--org)
;;; Custom-Set Variables
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file)

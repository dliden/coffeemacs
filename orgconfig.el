;;; Files and Directories
;;; Packages
;;;; Org mode base & config
(use-package org
  :defer t
  :after (jupyter)
  ;:ensure org-plus-contrib
  :commands (org-mode
             org-capture
             org-store-link)
  :hook
  (org-mode . (lambda() (set-fill-column 80)))
  ;(org-mode . turn-on-auto-fill)
  :config
  (general-define-key
   :keymaps 'global
   "C-c w" 'org-refile
   "C-c l" 'org-store-link
   "C-c c" 'org-capture
   "C-c a" 'org-agenda
   :keymaps 'org-mode-map
   "C-c C-q" 'counsel-org-tag
   )
    ;; Default org-mode startup
  (setq org-startup-folded t
        org-startup-with-inline-images nil
        org-startup-with-latex-preview nil
        org-startup-indented t
        org-indent-mode 1
        org-indent-indentation-per-level 1
        org-hide-emphasis-markers t
        org-pretty-entities t
        org-blank-before-new-entry '((heading . nil) (plain-list-item . nil))
        org-return-follows-link t
        org-cite-global-bibliography '("~/org/slip-box/bibliography/references.bib")
        org-preview-latex-image-directory (expand-file-name "latex_images/" org-directory)
        org-M-RET-may-split-line '((default . nil))
        ;;org-insert-heading-respect-content t
        org-log-done 'time
        org-log-into-drawer t
        org-enforce-todo-dependencies t
        )
  ;; org agenda
 (setq
  org-agenda-files
  (list
   org-directory
   (expand-file-name "planner/weekly_planners/" org-directory)
   (expand-file-name
    "denotes/personal/20240313T092608--plans__calendar_events_planner_travel.org"
    org-directory)
   (expand-file-name "denotes/journal/" org-directory))
  org-agenda-use-time-grid t
  org-agenda-show-current-time-in-grid t
  ;; add %l to indent according to org hierarchy
  org-agenda-prefix-format
    '((agenda . " %i %-12:c%?-12t% s")
          (todo . " %i %-12:c")
          (tags . " %i %-12:c")
          (search . " %i %-12:c"))
  org-agenda-start-on-weekday nil
  org-habit-show-all-today t
  org-agenda-skip-scheduled-if-done t
  org-agenda-skip-deadline-if-done t
  ;; Custom org agenda block views (WIP)
  org-agenda-custom-commands
  '(("j" "Journals"
     ((tags "persist")
      (todo))
       ((org-agenda-files
         (file-expand-wildcards "~/org/denotes/journal/*.org"))
        (org-agenda-prefix-format "%-2c:::"))))
  ;; hide tags
  org-agenda-hide-tags-regexp ".*")
  
  
  ;; Larger latex fragments
  (plist-put org-format-latex-options :scale 1.5)
  ;;org to-do keywords
  (setq org-todo-keywords 
        '((sequence "TODO(t)" "IN PROGRESS(p)" "|" "HOLD(h)" "DONE(d)"))
        org-use-fast-todo-selection t)
   ;; org-babel
  (org-babel-do-load-languages
   'org-babel-load-languages
   '(
     (emacs-lisp . t)
     (R          . t)
     (python     . t)
    ; (julia      . t)
    ; (julia-vterm . t)
     (org        . t)
     (dot        . t)
     (sql        . t)
     (sqlite     . t)
    ; (http       . t)
     (latex      . t)
     (js         . t)
     (shell      . t)
     (C          . t)
     (jupyter    . t)
     ))
  (setq org-babel-default-header-args '((:eval . "never-export")
                                        (:exports . "both")
                                        (:cache . "no")
                                        (:results . "replace"))
        org-src-fontify-natively t
        org-src-preserve-indentation t
        org-src-tab-acts-natively t
        org-src-window-setup 'current-window
        org-confirm-babel-evaluate nil)

  (eval-after-load 'org
  (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images))
  
  ;; Capture Templates
(setq org-capture-templates
      '(("i" "inbox" entry (file+headline "~/org/denotes/personal/20250226T094215--inbox__capture_quicknotes_todo.org" "Inbox")
         "** TODO %^{Name of Entry} %^g\n%?")
        ("j" "Journal entry" entry (file+datetree "~/org/planner/journal.org")
         "* %?"
         :empty-lines 1)
        ("d" "Dream entry" entry (file+datetree "~/org/planner/dreamjournal.org")
         "* %?")
        ("n" "paper-planner note" entry (file+headline paper-planner-current-file-path
                                                       "Quick Notes")
         "** TODO %^{Name of Entry} \n%?")
        ("t" "paper-planner todo" entry (file+headline paper-planner-current-file-path
                                                     "Quick Tasks")
         "** TODO %^{Name of Entry} %^g\n%?")
        ("r" "paper-planner reading list" entry (file+headline paper-planner-current-file-path
                                                            "Reading List")
         "** %^{Title} \nSuggested by: %^{Suggested by}\nSource: %^{Source}\n")))

  ;(add-hook 'org-capture-mode-hook #'org-align-tags t)
  ;; fig sizes
  (setq org-image-actual-width 400)
  (diminish 'org-indent-mode)
  )

;;;; Other org-related packages
;;;;;; M
(use-package mixed-pitch
  :diminish
  :config
  (add-hook 'org-mode-hook #'mixed-pitch-mode)
  (add-hook 'org-mode-hook #'buffer-face-mode)
  (set-face-attribute 'variable-pitch nil :family "Iosevka Aile"
  :height 150)
  (set-face-attribute 'fixed-pitch nil :family "Iosevka Fixed"
  :height 150))
;;;;;; O
(use-package org-download
  :disabled t
  :diminish
  :hook
  (add-hook 'dired-mode-hook 'org-download-enable)
  :config
  (setq-default org-download-image-dir "~/images/orgDL")
  )
(use-package org-fc
  :straight
  (org-fc
   :type git :repo "l3kn/org-fc"
   :files (:defaults "awk" "demo.org"))
  :custom
  (org-fc-directories '("~/org/denotes/"))
  :config
  (require 'org-fc-hydra)
  (global-set-key (kbd "C-c f") 'org-fc-hydra/body)
  (evil-define-minor-mode-key '(normal insert emacs) 'org-fc-review-flip-mode
  (kbd "RET") 'org-fc-review-flip
  (kbd "n") 'org-fc-review-flip
  (kbd "s") 'org-fc-review-suspend-card
  (kbd "q") 'org-fc-review-quit)

(evil-define-minor-mode-key '(normal insert emacs) 'org-fc-review-rate-mode
  (kbd "a") 'org-fc-review-rate-again
  (kbd "h") 'org-fc-review-rate-hard
  (kbd "g") 'org-fc-review-rate-good
  (kbd "e") 'org-fc-review-rate-easy
  (kbd "s") 'org-fc-review-suspend-card
  (kbd "q") 'org-fc-review-quit))

(use-package org-present
  :after org
  :hook ((org-present-mode . (lambda ()
                               (org-present-big)
                               (org-display-inline-images)
                               (org-present-hide-cursor)
                               (org-present-read-only)))
         (org-present-mode-quit . (lambda ()
                                    (org-present-small)
                                    (org-remove-inline-images)
                                    (org-present-show-cursor)
                                    (org-present-read-write)))))

  
(use-package org-tree-slide
  :config
  ;; Closure to create play and stop hooks with a local variable for header line format
  (let ((original-header-line-format nil))
    (defun my-org-tree-slide-play-hook ()
      (text-scale-set 1)
      (olivetti-mode 1)
      (olivetti-set-width 60)
      ;; Save and hide the header line
      (setq original-header-line-format header-line-format
            header-line-format nil
            org-tree-slide-header nil))

    (defun my-org-tree-slide-stop-hook ()
      (text-scale-set 0)
      (olivetti-mode -1)
      ;; Restore the original header line format
      (setq header-line-format original-header-line-format
            org-tree-slide-header t)))

  (setq org-tree-slide-heading-emphasis t)

  :hook ((org-tree-slide-play . my-org-tree-slide-play-hook)
         (org-tree-slide-stop . my-org-tree-slide-stop-hook)))

;;;;;; P
(use-package paper-planner
  ;; my package!
  :straight (paper-planner :type git :host github :repo "djliden/paper-planner")
  :config
  ;; Configure your settings here
  (setq paper-planner-file-format "weekly-planner-%s.org")
  (setq paper-planner-starting-day 'sunday)
  (setq paper-planner-my-tasks
        '(
          (h1 . 5)
          (h2 . 5)
          (h3 . 5)
          (h4 . 5)
          ))
  ;; Optional: Set the default directory for your planner files
  (setq paper-planner-directory "~/org/planner/weekly_planners/")
  ;; Set global variable for current paper planner filename
  (paper-planner-current-file t))

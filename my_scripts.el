;; Scripts for my personal workflow and for learning more elisp
;; you can collect arguments with "n" for numeric and "s" for string.
;; great resources on user input here http://xahlee.info/emacs/emacs/elisp_idioms_prompting_input.html



(defun roll (sides &optional times)
  (interactive "nHow Many Sides?\nnHow Many Times?")
  "Roll a 'sides'-sided die 'times' times"
  (or times (setq times 1))
  (random t)
  (setq out_list '())
  (while ( > times 0)
    (push (random sides) out_list)
    (setq times (- times 1))
    )
  (flatten-list out_list)
  )

;; Here's how we can associate activities with rolls
;; (cl-pairlis '(study read chores) (roll 12 3))

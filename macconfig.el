;; This file contains configuration options specific to using emacs on a mac.

;;;; Startup -- get Path
;;(use-package exec-path-from-shell
;;  :init
;;  (exec-path-from-shell-initialize)
;;  )
(setenv "PATH" (concat "/Library/TeX/texbin:" (getenv "PATH")))
(setq exec-path (append '("/Library/TeX/texbin") exec-path))

;;;; Key Bindings
(setq mac-command-modifier 'meta)


# coffeemacs

This is my personal configuration for emacs. I have been using various cobbled-together init.el files and I've used spacemacs in the past. The goal of this project is to generate a personalized configuration that is tailored to my specific needs.